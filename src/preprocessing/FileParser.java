package preprocessing;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class FileParser {

	private Path pathFiles, outputDir, resultDir;
	public static final String OUTPUT_DIRECTORY = "Doxygen_Parsed";
	public static final String RESULT_DIRECTORY = "Results";


	public FileParser(String pathfiles){
		this.pathFiles = FileSystems.getDefault().getPath(pathfiles);
		this.outputDir = FileSystems.getDefault().getPath(pathfiles+File.separator+OUTPUT_DIRECTORY);
		this.resultDir = FileSystems.getDefault().getPath(pathfiles+File.separator+OUTPUT_DIRECTORY+File.separator+RESULT_DIRECTORY);
	}

	public void renameFiles() throws IOException{
		Path dir = pathFiles;
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
			outputDir.toFile().mkdir();
			resultDir.toFile().mkdir();
			String lastMethod = "";
			String originalFileName, fileName, newString;
			int nMethod = 0;
			String[] tokens;
			for (Path path: stream) {
				originalFileName = path.getFileName().toString();
				if(originalFileName.startsWith("class")&&originalFileName.endsWith(".dot")){
					newString="";
					fileName = originalFileName.substring(5);
					fileName = fileName.replace("_1_1", "_");
					tokens = fileName.split("_");
					for(int i = 0;i<tokens.length-2;i++){
						newString = newString.concat(tokens[i]+"_");
					}
					//TODO approccio if-else da replicare per la scrittura delle info relative alla stessa classe
					if(lastMethod.equalsIgnoreCase(newString)){
						nMethod++;
					}
					else{
						nMethod = 0;
						lastMethod = newString;
					}
					newString = newString.concat("method"+nMethod+".dot");
					Files.copy(path, outputDir.resolve(newString), StandardCopyOption.REPLACE_EXISTING);
				}
			}
		}
	}
	
	/**
	 * @return the pathFiles
	 */
	public Path getPathFiles() {
		return pathFiles;
	}

	/**
	 * @param pathFiles the pathFiles to set
	 */
	public void setPathFiles(Path pathFiles) {
		this.pathFiles = pathFiles;
	}

	/**
	 * @return the outputDir
	 */
	public Path getOutputDir() {
		return outputDir;
	}

	/**
	 * @param outputDir the outputDir to set
	 */
	public void setOutputDir(Path outputDir) {
		this.outputDir = outputDir;
	}
	
}
