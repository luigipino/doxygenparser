package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;

import grammar.DoxygenGrammarSpecs;
import grammar.ParseException;
import preprocessing.FileParser;
import util.ClassWrapper;
import util.FileWriter;
import util.MethodWrapper;

public class DoxygenParser {

	public static void main(String[] args) {
		//String dirPath = "D:/Eclipse Workspaces/mag workspace/UnisannioWeather/Server/UnisannioWeatherServer/html";
		String dirPath = "C:/Users/Alessandro/Desktop/html";
		String lastClass = "", currentClass;
		int i = 0;
		FileWriter writer;
		FileInputStream fis = null;
		DoxygenGrammarSpecs dg = null;
		ClassWrapper cw = null;
		MethodWrapper mw;
		FileParser fp = new FileParser(dirPath);
		boolean firstClass = true;
		try {
			fp.renameFiles();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Path dir = fp.getOutputDir();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
			for (Path path: stream) { //non dobbiamo fare controlli sul nome del file, perch� siamo nella directory generata in pre-processing
				i++;
				if(path.toFile().isDirectory())
					continue;
				fis = new FileInputStream(path.toFile());
				currentClass = getClassNameFromFile(path.getFileName().toString());
				if(!currentClass.equalsIgnoreCase(lastClass)){
					lastClass = currentClass;
					if(!firstClass){
						writer = new FileWriter(cw, dirPath+File.separator+FileParser.OUTPUT_DIRECTORY+File.separatorChar+FileParser.RESULT_DIRECTORY);
						writer.write();
					}
					else firstClass = false;
					cw = new ClassWrapper(lastClass);
				}
				dg = new DoxygenGrammarSpecs(fis);
				dg.init();
				mw = dg.start();
				mw.printCalls();
				cw.addMethod(mw);
				System.out.println(mw.getMethodName());
			}
			writer = new FileWriter(cw, dirPath+File.separator+FileParser.OUTPUT_DIRECTORY+File.separatorChar+FileParser.RESULT_DIRECTORY);
			writer.write();
			System.out.println("File generati: "+i);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private static String getClassNameFromFile(String fileName){
		String[] tokens = fileName.split("_");
		String className="";
		for(int i = 0; i<tokens.length-1; i++){
			className += tokens[i]+"_";
		}
		return className.substring(0,className.length()-1);
	}

}
