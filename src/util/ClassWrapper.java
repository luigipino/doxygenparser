package util;

import java.util.ArrayList;
import java.util.List;

public class ClassWrapper {

	private String className;
	private List<MethodWrapper> referencedMethods;
	
	public ClassWrapper(String className) {
		super();
		this.className = className;
		referencedMethods = new ArrayList<MethodWrapper>();
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the referencedMethods
	 */
	public List<MethodWrapper> getReferencedMethods() {
		return referencedMethods;
	}

	/**
	 * @param referencedMethods the referencedMethods to set
	 */
	public void setReferencedMethods(List<MethodWrapper> referencedMethods) {
		this.referencedMethods = referencedMethods;
	}

	public void addMethod(MethodWrapper mw){
		referencedMethods.add(mw);		
	}
	
	public boolean containsMethod(String methodName){
		for(MethodWrapper mw : referencedMethods){
			if(mw.getMethodName().equalsIgnoreCase(methodName)) return true;
		}
		return false;
	}
	
}
