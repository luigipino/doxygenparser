package util;

import java.util.ArrayList;
import java.util.List;

public class MethodWrapper {
	
	private String methodName;
	private List<ReferenceCall> calls;

	
	
	public MethodWrapper() {
		super();
		calls = new ArrayList<ReferenceCall>();
	}

	public MethodWrapper(String methodName) {
		super();
		this.methodName = methodName;
		calls = new ArrayList<ReferenceCall>();
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	/**
	 * @return the calls
	 */
	public List<ReferenceCall> getCalls() {
		return calls;
	}

	/**
	 * @param calls the calls to set
	 */
	public void setCalls(List<ReferenceCall> calls) {
		this.calls = calls;
	}
	
	public void addCall(ReferenceCall rc){
		calls.add(rc);		
	}
	
	public boolean containsCall(String calledMethod){
		for(ReferenceCall rc : calls){
			if(rc.getMethodName().equalsIgnoreCase(calledMethod)) return true;
		}
		return false;
	}

	public void printCalls(){
		System.out.println("ReferencedCalls");
		for(ReferenceCall c: calls){
			System.out.println(c.getClassName() +"******"+ c.getMethodName()+"****"+c.isDirect());
		}
		System.out.println("*******");
	}
	
}

