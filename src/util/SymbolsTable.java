package util;

import java.util.HashMap;
import java.util.Map;

public class SymbolsTable {

	private Map<String, String> symbols;

	public SymbolsTable(){
		symbols = new HashMap<String, String>();
	}

	/**
	 * @return the symbols
	 */
	public Map<String, String> getSymbols() {
		return symbols;
	}
	/**
	 * @param symbols the symbols to set
	 */
	public void setSymbols(Map<String, String> symbols) {
		this.symbols = symbols;
	}
	
	public void addSymbol(String key, String value){
		symbols.put(key, value);
	}
	
	public void addSymbol(String key){
		symbols.put(key, "");
	}
	
	public boolean contains(String key){
		return symbols.containsKey(key);
	}
	
	public String getSymbolByKey(String key){
		return symbols.get(key);
	}
	
	public void printValues(){
		for(String s: symbols.keySet()){
			System.out.println("key: "+ s+ " value: "+symbols.get(s));
		}
	}
}
