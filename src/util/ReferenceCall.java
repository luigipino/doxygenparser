package util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReferenceCall {
	
	private String className, methodName;
	private boolean direct;
	private static Pattern pattern = Pattern.compile("Node\\d+");

	public ReferenceCall(String className, String methodName, boolean direct) {
		super();
		this.className = className;
		this.methodName = methodName;
		this.direct = direct;
	}
	
	public ReferenceCall(String label, boolean direct) {
		super();
		Matcher m = pattern.matcher(label);
		if(m.matches()){
			this.className = "";
			this.methodName = label;
		}
		else parseLabel(label);
		this.direct = direct;
	}

	private void parseLabel(String label){
		label = label.substring(1,label.length()-1);
		String[] splitted = label.split("\\.");
		System.out.println(label);
		System.out.println(splitted.length);
		this.methodName = splitted[splitted.length-1];
		String className = "";
		for(int i=0; i<splitted.length-1;i++){
			className += splitted[i]+".";
		}
		className = className.replace("\\l", "");
		this.className = className.substring(0, className.length()-1);
	}
	
	public void setValuesByLabel(String label){
		this.parseLabel(label);
	}
	
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * @return the methodName
	 */
	public String getMethodName() {
		return methodName;
	}

	/**
	 * @param methodName the methodName to set
	 */
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public boolean isDirect() {
		return direct;
	}

	public void setDirect(boolean direct) {
		this.direct = direct;
	}
	
	public static void main(String[] args) {
		String label ="\"com.unisannio.it.Codabar\\lBarcode.methodName\"";
		ReferenceCall rf = new ReferenceCall(label, true);
		System.out.println(rf.getClassName());
		System.out.println(rf.getMethodName());
	}
	
}
