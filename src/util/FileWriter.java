package util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class FileWriter {

	private ClassWrapper clazz;
	private PrintWriter writer;
	
	public FileWriter(ClassWrapper clazz,String path) throws IOException{
		this.clazz = clazz;
		File file = new File(path+File.separatorChar+clazz.getClassName()+".txt");
		if(!file.exists()){
			file.createNewFile();
		}
		this.writer = new PrintWriter(new FileOutputStream(file));
	}

	public ClassWrapper getClazz() {
		return clazz;
	}

	public void setClazz(ClassWrapper clazz) {
		this.clazz = clazz;
	}
	
	public void write(){
		writer.println("Class: "+ clazz.getClassName().replace("_", "."));
		for(MethodWrapper method: clazz.getReferencedMethods()){
			writer.println("\tMethod: "+ method.getMethodName()+" ->");
			for(ReferenceCall call: method.getCalls()){
				String str;
				if(call.isDirect()) str = " direct call";
				else str = " undirect call";
				writer.println("\t\tto "+call.getClassName()+" . "+call.getMethodName()+" "+str);
			}
			writer.println();
		}
		writer.flush();
		writer.close();
	}
}
